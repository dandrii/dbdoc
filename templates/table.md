## {{.Table.Name}}

| Schema            |
| ----------------- |
| {{.Table.Schema}} |

{{.Table.Description.String}}

### Columns

| Name | Type | Description |
| ---- | ---- | ----------- |

{{- range .Columns}}
| {{.Name}} | {{.Type}} | {{.Description.String}} |
{{- end}}
