package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/dandrii/dbdoc/config"
)

var selectTables = `
SELECT
	t.tablename, t.schemaname, obj_description(c.oid)
FROM
	pg_class AS c
LEFT JOIN pg_catalog.pg_tables AS t ON c.relname = t.tablename
WHERE
	c.relkind = 'r'
	AND t.schemaname NOT IN ('pg_catalog', 'information_schema')
`

var selectColumns = `
SELECT
	col.column_name, col.udt_name, des.description
FROM
	information_schema.columns AS col
LEFT JOIN pg_class AS cls ON cls.relname = $1
LEFT JOIN pg_catalog.pg_description AS des ON des.objoid = cls.oid AND des.objsubid = col.ordinal_position
WHERE
	col.table_name = $1
`

type column struct {
	Name        string         `db:"column_name"`
	Type        string         `db:"udt_name"`
	Description sql.NullString `db:"description"`
}

type table struct {
	Name        string         `db:"tablename"`
	Schema      string         `db:"schemaname"`
	Description sql.NullString `db:"obj_description"`
}

// GetTables returns array of tables
func GetTables(db *sqlx.DB) []config.Table {
	ts := []table{}

	err := db.Select(&ts, selectTables)
	if err != nil {
		log.Fatalln(err)
	}

	r := []config.Table{}
	for _, t := range ts {
		r = append(r, config.Table(t))
	}
	return r
}

// GetColumns returns array of columns for given table name
func GetColumns(db *sqlx.DB, tname string) []config.Column {
	cs := []column{}

	err := db.Select(&cs, selectColumns, tname)
	if err != nil {
		log.Fatalln(err)
	}

	r := []config.Column{}
	for _, c := range cs {
		if c.Type[0] == '_' {
			c.Type = c.Type[1:] + "[]"
		}
		r = append(r, config.Column(c))
	}
	return r
}

// AddCommentOnColumn adds comment for given column
func AddCommentOnColumn(db *sqlx.DB, tname string, colname string, cmnt string) (err error) {
	_, err = db.Exec(fmt.Sprintf("COMMENT ON COLUMN \"%s\".\"%s\" IS '%s'", tname, colname, strings.ReplaceAll(cmnt, "'", "''")))
	return
}

// AddCommentOnTable adds comment for given table
func AddCommentOnTable(db *sqlx.DB, tname string, cmnt string) (err error) {
	_, err = db.Exec(fmt.Sprintf("COMMENT ON TABLE \"%s\" IS '%s'", tname, strings.ReplaceAll(cmnt, "'", "''")))
	return
}
