# DB Doc

## Usage

```sh
./dbdoc --conn postgres://postgres:postgres@localhost/postgres
```

Generated files will be located under `out/` directory. Every table will have separate `[tableName].md` file.

You can provide additional comments via CSV file and reference it using `--comm-csv` option. Referenced CSV file should have 3 columns. To add comment to table itself leave second column empty, e.g.

| table | column | description                         |
| ----- | ------ | ----------------------------------- |
| users |        | All users registered in application |
| users | id     | User ID                             |
