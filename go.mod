module gitlab.com/dandrii/dbdoc

go 1.16

require (
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.2.0
)
