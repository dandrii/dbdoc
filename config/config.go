package config

import (
	"database/sql"
	"flag"
)

// ConnectionString holds DB connection string
var ConnectionString = flag.String("conn", "", "DB connection string")

// AdditionalComments holds path to CSV file with comments
var AdditionalComments = flag.String("comm-cvs", "", "Path to CSV file with comments")

// Column holds column name, type and description
type Column struct {
	Name        string
	Type        string
	Description sql.NullString
}

// Table holds table name, description and columns
type Table struct {
	Name        string
	Schema      string
	Description sql.NullString
}

func init() {
	flag.Parse()
}
