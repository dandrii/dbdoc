package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"text/template"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/dandrii/dbdoc/config"
	"gitlab.com/dandrii/dbdoc/postgres"
)

type view struct {
	Table   config.Table
	Columns []config.Column
}

func main() {
	db, err := sqlx.Connect("postgres", *config.ConnectionString)
	if err != nil {
		log.Fatalln(err)
	}

	if *config.AdditionalComments != "" {
		log.Println("adding comments from CSV")

		f, err := os.Open(*config.AdditionalComments)
		if err != nil {
			log.Fatalln(err)
		}
		r := csv.NewReader(f)
		r.ReuseRecord = true

		i := 0
		for {
			i++

			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatal(err)
			}

			if i == 1 {
				// skip header of CSV
				continue
			}

			t, col, desc := record[0], record[1], record[2]
			if col == "" {
				err = postgres.AddCommentOnTable(db, t, desc)
			} else {
				err = postgres.AddCommentOnColumn(db, t, col, desc)
			}

			if err != nil && !strings.Contains(err.Error(), "does not exist") {
				log.Fatalln(err)
			}

			fmt.Printf("\r%d", i)
		}
	}

	tpl, err := template.ParseFiles("templates/table.md", "templates/index.html")
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("reading table definitions")
	tables := postgres.GetTables(db)

	log.Printf("found %d tables", len(tables))

	buf := bytes.Buffer{}
	for _, t := range tables {
		fname := fmt.Sprintf("out/%s.md", t.Name)
		log.Printf("creating %s", fname)

		f, err := os.Create(fname)
		if err != nil {
			log.Fatalln(err)
		}

		log.Printf("reading column definitions for %s", t.Name)
		cols := postgres.GetColumns(db, t.Name)

		w := io.MultiWriter(&buf, f)

		log.Printf("executing template for %s", t.Name)
		tpl.ExecuteTemplate(w, "table.md", view{
			Table:   t,
			Columns: cols,
		})
	}

	log.Println("generating single HTML")
	f, err := os.Create("out/index.html")
	if err != nil {
		log.Fatalln(err)
	}

	tpl.ExecuteTemplate(f, "index.html", strings.ReplaceAll(buf.String(), "`", "\\`"))
}
